import json
import pytest
from feiraslivres import create_app

@pytest.fixture
def client(request):
    app = create_app("dev")
    test_client = app.test_client()

    return test_client


def test_get_feiras(client):
    res = client.get('/feiras')
    assert res.status_code == 200

def test_get_feiras_filter(client):
    res = client.get('/feiras?bairro=santana')
    assert res.status_code == 200

def test_create_feiras(client):
    res = client.post('/feiras', data={"registro": "TESTE"})
    assert res.status_code == 201

def test_create_invalid_feiras(client):
    res = client.post('/feiras', data={"nome_feira": "BATATA"})
    assert res.status_code == 400

def test_get_feira(client):
    res = client.get('/feira/TESTE')
    assert res.status_code == 200

def test_get_invalid_feira(client):
    res = client.get('/feira/BATATA')
    assert res.status_code == 404

def test_create_duplicate_feiras(client):
    res = client.post('/feiras', data={"registro": "TESTE"})
    assert res.status_code == 400

def test_put_empty_feira(client):
    res = client.put('/feira/TESTE2', data={"nome_feira": "Teste 2"})
    assert res.status_code == 201 

def test_put_existing_feira(client):
    res = client.put('/feira/TESTE2', data={'nome_feira': "Feira da Fruta"})
    assert res.status_code == 200 

def test_delete_feira(client):
    res = client.delete('/feira/TESTE')
    assert res.status_code == 204
    res = client.delete('/feira/TESTE2')
    assert res.status_code == 204

def test_delete_invalid(client):
    res = client.delete('/feira/TESTE')
    assert res.status_code == 404

