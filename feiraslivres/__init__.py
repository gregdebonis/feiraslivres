import logging
from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from sqlalchemy import create_engine, exc


DB_COLS = [
  'long',
  'lat',
  'setcens',
  'areap',
  'coddist',
  'distrito',
  'codsubpref',
  'subprefe',
  'regiao5',
  'regiao8',
  'nome_feira',
  'registro',
  'logradouro',
  'numero',
  'bairro',
  'referencia',
]

SEARCH_COLS = [
  'distrito',
  'regiao5',
  'nome_feira',
  'bairro',
]

e = create_engine('sqlite:///db.sqlite3')

class FeiraList(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        for col in SEARCH_COLS: parser.add_argument(col)
        args = parser.parse_args()
        args = {k.upper(): v.upper() for k, v in args.items() if v}
        if 'REGIAO5' in args: args['REGIAO5'] = args['REGIAO5'].title()

        conn = e.connect()
        query = "select * from feiras"
        if args:
            query += ' where '
            query += ' and '.join([f"{k}='{v}'" for k, v in args.items()])

        res = conn.execute(query)
        return [(dict(row.items())) for row in res]

    def post(self):
        parser = reqparse.RequestParser()
        for col in DB_COLS: parser.add_argument(col)
        args = parser.parse_args()
        
        columns = ', '.join(args.keys())
        placeholders = ':'+', :'.join(args.keys())
        query = 'INSERT INTO feiras (%s) VALUES (%s)' % (columns, placeholders)

        try:
            conn = e.connect()
            res = conn.execute(query, args)
            
            return args, 201
        except exc.IntegrityError as err:
            return {
              "errors": [
                {
                  "status": "400",
                  "title": "Integrity Error",
                  "detail": "field 'registro' is required and must be unique",
                }
              ]
            }, 400
        

class Feira(Resource):

    def get(self, registro):
        conn = e.connect()
        
        res = conn.execute("select * from feiras where REGISTRO=:registro", {'registro': registro})

        result = [(dict(row.items())) for row in res]
        if not result: 
            return {
              "errors": [
                {
                  "status": 404,
                  "title": "Resource not found",
                }
              ]
            }, 404

        return result, 200


    def delete(self, registro):
        conn = e.connect()

        res = conn.execute("delete from feiras where REGISTRO=:registro", {'registro': registro})
        if res.rowcount != 1:
            return {
              "errors": [
                {
                  "status": 404,
                  "title": "Resource not found",
                }
              ]
            }, 404

        return {}, 204


    def put(self, registro):
        # upsert it not supported in sqlite < 3.24.0
        
        parser = reqparse.RequestParser()
        for col in DB_COLS: parser.add_argument(col)
        args = parser.parse_args()
        
        args['registro'] = registro
        
        columns = ', '.join(args.keys())
        placeholders = ':'+', :'.join(args.keys())
        query = 'INSERT INTO feiras (%s) VALUES (%s)' % (columns, placeholders)

        try:
            conn = e.connect()
            res = conn.execute(query, args)
            
            return args, 201

        except exc.IntegrityError as err:
            # error inserting, should update instead
            pass

        to_update = {k:v for k, v in args.items() if v and k != 'registro'}
        qs = ', '.join([f'{k}=:{k}' for k in to_update])
        query = 'UPDATE feiras set %s WHERE registro=:registro' % (qs)

        conn = e.connect()
        res = conn.execute(query, args)
        
        return args, 200

def create_app(config):

    app = Flask(__name__)
    api = Api(app)

    api.add_resource(Feira, '/feira/<string:registro>')
    api.add_resource(FeiraList, '/feiras')

    logging.basicConfig(filename='db.log')
    logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

    return app

if __name__ == '__main__':
    app = create_app()
    app.run()

