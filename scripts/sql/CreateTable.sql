CREATE TABLE IF NOT EXISTS feiras (
  id integer PRIMARY KEY,
  long integer,
  lat integer,
  setcens integer,
  areap integer,
  coddist integer,
  distrito text,
  codsubpref integer,
  subprefe text,
  regiao5 text,
  regiao8 text,
  nome_feira text,
  registro text UNIQUE NOT NULL,
  logradouro text,
  numero text,
  bairro text,
  referencia text
);
