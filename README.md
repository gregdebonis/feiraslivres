# feiraslivres

API localizadora de Feiras Livres na cidade de São Paulo

## Detalhes Técnicos

O código foi desenvolvido e testado em Python 3.7, com a biblioteca [Flask-RESTful](https://flask-restful.readthedocs.io/en/latest/).
O banco de dados utilizado é o [SQLite3](https://www.sqlite.org).

Logs de acesso ao banco são gravados no arquivo `db.log`.


### Requisitos

* Python
* SQLite3

```
pip install -r requirements.txt
```


### Inicialização

Para criar o banco de dados, importar os dados iniciais e rodar localmente o serviço, use:

```
sh scripts/create_db.sh
sh scripts/load_data.sh
FLASK_APP=feiraslivres flask run
```

## Exemplos de Chamadas

`GET /feiras` - Lista de todas as feiras

`GET /feiras?bairro=[]&nome_feira=[]&regiao5=[]&distrito=[]` - Filtra as feiras

`POST /feiras/` - Cria uma feira nova (*registro* é um dado necessário)

`GET /feira/[registro]` - Informações sobre uma feira específica

`DELETE /feira/[registro]` - Remove uma feira

`PUT /feira/[registro]` - Cria/atualiza uma feira


## Testes

```
coverage run -m pytest
coverage report feiraslivres/__init__.py 
```

## Considerações Finais

Próximos passos:
* Arrumar a estrutura do projeto (não estou feliz com todo o código no `__init__.py` do módulo)
* Melhorar os testes - estou checando apenas códigos de retorno, e mesmo assim de uma maneira bem precária. Vale criar umas fixtures e fazer uma suite bem mais robusta.


Aproveitei a oportunidade para utilizar pela primeira vez a biblioteca [Flask-RESTful](https://flask-restful.readthedocs.io/en/latest/). A experiência foi interessante, mas em alguns momentos a documentação (principalmente no que diz respeito a integração com pytest) deixou um pouco a desejar... Ou eu que me perdi um pouco, mesmo. :)
