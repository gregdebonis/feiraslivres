# conftest.py
import pytest

from feiraslivres import create_app


@pytest.fixture
def app():
    test_app = app.app
    test_app.debug = True
    return test_app
